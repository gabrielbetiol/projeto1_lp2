<?php

class Projeto extends CI_controller
{
    
    public function index()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v[]           = array();
        $v['carousel'] = $this->load->view('projeto/contents/carousel', '', true);
        
        $this->load->model('CardModel');
        
        $v['noticia'] = $this->CardModel->info_cards();
        $this->load->view('projeto/layouts/layout_home', $v);
        
        $this->load->view('common/footer');
    }
    public function produtos()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v[]         = array();
        $v['titulo'] = "Todos os Produtos";
        $v['tipo']   = "geral";
        
        $this->load->model('ProdutoModel');
        
        $v['produto'] = $this->ProdutoModel->product_cards($v['tipo']);
        $this->load->view('projeto/layouts/layout_produto', $v);
        
        $this->load->view('common/footer');
    }
    public function all_star()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v[]         = array();
        $v['titulo'] = "All-Star";
        $v['tipo']   = "allstar";
        
        $this->load->model('ProdutoModel');
        
        $v['produto'] = $this->ProdutoModel->product_cards($v['tipo']);
        $this->load->view('projeto/layouts/layout_produto', $v);
        
        $this->load->view('common/footer');
    }
    public function kids()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v[]         = array();
        $v['titulo'] = "Kids";
        $v['tipo']   = "kids";
        
        $this->load->model('ProdutoModel');
        
        $v['produto'] = $this->ProdutoModel->product_cards($v['tipo']);
        
        $this->load->view('projeto/layouts/layout_produto', $v);
        
        $this->load->view('common/footer');
    }
    public function detalhes()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        
        $this->load->view('teste');
        
        $this->load->view('common/footer');
    }
    public function preços()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('common/footer');
        echo 'Preço do produto selecionado';
    }
    public function sobre()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['content'] = $this->load->view('projeto/contents/sobre', '', true);
        
        $this->load->view('projeto/layouts/layout_info.php', $v);
        
        $this->load->view('common/footer');
    }
    public function contato()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['content'] = $this->load->view('projeto/contents/fale_conosco', '', true);
        
        $this->load->view('projeto/layouts/layout_info.php', $v);
        
        $this->load->view('common/footer');
    }
    public function alterar()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['title'] = 'Alterar Produtos';
        
        $this->load->model('ProdutoModel', 'model');
        $v['content'] = $this->model->lista();
        $this->load->view('projeto/layouts/products_view', $v);
        $this->load->view('projeto/contents/insere');
        
        $this->load->view('common/footer');
        
    }
    public function edit($id)
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['title'] = 'Alterar Produtos';
        $v['acao']  = 'Alterar';
        
        $this->load->model('ProdutoModel', 'model');
        $v['produto'] = $this->model->carrega_produto($id);
        
        $v['content'] = $this->load->view('projeto/contents/editar', $v, true);
        $this->load->view('projeto/layouts/products_view', $v);
        
        
        $this->load->view('common/footer');
        
    }
    public function inserir()
    {
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $v['title'] = 'Inserir Produtos';
        $v['acao']  = 'Inserir';
        
        $this->load->model('ProdutoModel', 'model');
        $this->model->inserir();
        
        $v['content'] = $this->load->view('projeto/contents/editar', $v, true);
        $this->load->view('projeto/layouts/products_view', $v);
        
        
        $this->load->view('common/footer');
        
    }
    public function deletar($id)
    {
        $this->load->model('ProdutoModel', 'model');
        $this->model->delete($id);
        redirect('projeto/alterar');
    }
}