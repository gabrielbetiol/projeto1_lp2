<?php

class Product
{
    
    private $tipo;
    private $img;
    private $title;
    private $colecao;
    private $cor;
    private $material;
    private $preco;
    
    private $db;
    
    function __construct($tipo = null, $img = null, $title = null, $colecao = null, $cor = null, $material = null, $preco = null)
    {
        $this->tipo     = $tipo;
        $this->img      = $img;
        $this->title    = $title;
        $this->colecao  = $colecao;
        $this->cor      = $cor;
        $this->material = $material;
        $this->preco    = $preco;
        
        $ci =& get_instance();
        $this->db = $ci->db;
    }
    
    public function getAll()
    {
        $res = $this->db->get('produto');
        return $res->result_array();
    }
    
    public function getById($id)
    {
        $rs = $this->db->get_where('produto', "id = $id");
        return $rs->row_array();
    }
    
    public function update($data, $id)
    {
        $this->db->update('produto',$data , "id = $id");
        return $this->db->affected_rows();
    }

    public function delete($id){
        $this->db->delete('produto',"id = $id");
    }
}
?>