<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CardModel extends CI_Model
{
    
    
    public function card_data()
    {
        $data = $this->db->get('card');
        return $data->result_array();
    }
    public function info_cards()
    {
        $data    = $this->card_data();
        $noticia = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            array_push($noticia, $this->load->view('projeto/contents/card', $data[$i], true));
        }
        $v['noticia'] = $noticia;
        return $v['noticia'];
    }
}
?>