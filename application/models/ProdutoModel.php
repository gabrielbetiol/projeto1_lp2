<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'libraries/Product.php';

class ProdutoModel extends CI_Model
{
    
    public function product_data($tipo)
    {
        // $sql = "SELECT * FROM produto WHERE id = $id";
        // $res = $this->db->query($sql);
        // $prod = $res->result_array();
        
        // return $prod[0];
        if ($tipo == 'geral') {
            $prod = $this->db->get('produto');
            return $prod->result_array();
        } else if ($tipo == 'allstar') {
            $prod = $this->db->get_where('produto', array(
                'tipo' => $tipo
            ));
            return $prod->result_array();
        } else if ($tipo == 'kids') {
            $prod = $this->db->get_where('produto', array(
                'tipo' => $tipo
            ));
            return $prod->result_array();
        }
        // $prod = $this->db->get_where('produto', $tipo) );
        //     return $prod->result_array();
        
    }
    
    public function carrega_produto($id)
    {
        $product = new Product;
        return $product->getById($id);
    }
    
    public function product_cards($tipo)
    {
        $prod    = $this->product_data($tipo);
        $produto = array();
        for ($i = 0; $i < sizeof($prod); $i++) {
            array_push($produto, $this->load->view('projeto/contents/card', $prod[$i], true));
        }
        $v['produto'] = $produto;
        return $v['produto'];
    }
    
    public function inserir()
    {
        if (sizeof($_POST) == 0)
            return;
        // $data = $this->input->post();
        // print_r($data);
        
        $tipo     = $this->input->post('tipo');
        $img      = $this->input->post('img');
        $title    = $this->input->post('title');
        $colecao  = $this->input->post('colecao');
        $cor      = $this->input->post('cor');
        $material = $this->input->post('material');
        $preco    = $this->input->post('preco');
        
        $product = new Product($tipo, $img, $title, $colecao, $cor, $material, $preco);
        $product->save();
        
    }
    
    public function atualizar($id)
    {
        if (sizeof($_POST) == 0)
            return;
        $data = $this->input->post();
        $user = new Product();
        
        if ($user->update($data, $id)) {
            redirect('projeto/alterar');
        }
        
    }
    
    public function lista()
    {
        $html    = '';
        $product = new Product();
        
        $data = $product->getAll();
        
        $html .= '<table class="table-responsive-lg text-nowrap table-bordered">
                        <thead class="black white-text">
                        <tr>
                            <th scope="col" class="text-center"><h5>Imagem</h5></th>
                            <th scope="col" class="text-center"><h5>Título</h5></th>
                            <th scope="col" class="text-center"><h5>Tipo</h5></th>
                            <th scope="col" class="text-center"><h5>Colecao</h5></th>
                            <th scope="col" class="text-center"><h5>Cor</h5></th>
                            <th scope="col" class="text-center"><h5>Material</h5></th>
                            <th scope="col" class="text-center"><h5>Preco</h5></th>
                            <th scope="col" class="text-center"><h5>Alterar/Excluir</h5></th>
                        </tr>
                        </thead>';
        foreach ($data as $row) {
            $html .= '<tr>';
            $html .= '<td class="text-center">' . $this->get_img_local($row['img']) . '</td>';
            $html .= '<td class="text-center">' . $row['title'] . '</td>';
            $html .= '<td class="text-center">' . $row['tipo'] . '</td>';
            $html .= '<td class="text-center">' . $row['colecao'] . '</td>';
            $html .= '<td class="text-center">' . $row['cor'] . '</td>';
            $html .= '<td class="text-center">' . $row['material'] . '</td>';
            $html .= '<td class="text-center">' . $row['preco'] . '</td>';
            $html .= '<td class="text-center">' . $this->get_icons($row['id']) . '</td></tr>';
        }
        
        $html .= '</table>';
        return $html;
    }
    
    private function get_icons($id)
    {
        $html = '<a href="' . base_url('projeto/edit/' . $id) . '"><i class="fas fa-pen mr-3 fa-lg text-dark"></i></a>';
        $html .= '<a href="' . base_url('projeto/deletar/' . $id) . '"><i class="fas fa-trash red-text fa-lg"></i></a>';
        return $html;
    }
    
    private function get_img_local($img)
    {
        $html = '<img class="img-thumbnail img-fluid" src="' . base_url('assets/images/' . $img . '.jpg') . '" alt="Imagens dos Produtos">';
        return $html;
    }
    
    public function delete($id)
    {
        $product = new Product();
        $product->delete($id);
    }
}
?>