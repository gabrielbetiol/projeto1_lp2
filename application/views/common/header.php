<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>PROJETO LPII - CONVERSE</title>
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/principal.css')?>" rel="stylesheet">

  
</head>

<body>