<nav class="mb-1 navbar navbar-expand-lg navbar-dark black ">
  <a class="navbar-brand" href="<?php echo base_url('projeto/index') ?>"> <img src="<?php echo base_url('assets/images/converse_logo.png'); ?>" class="img-fluid logo" width="300px" alt="logo converse"> </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle " id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Produtos
        </a>
        <div class="dropdown-menu dropdown-default elegant-color-dark " aria-labelledby="navbarDropdownMenuLink-333">
        <a class="dropdown-item white-text" href="<?php echo base_url('projeto/produtos') ?>">Todos os Produtos</a>
          <a class="dropdown-item white-text" href="<?php echo base_url('projeto/all_star') ?>">All-Star</a>
          <a class="dropdown-item white-text" href="<?php echo base_url('projeto/kids') ?>">Kids</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Empresa
        </a>
        <div class="dropdown-menu dropdown-default elegant-color-dark" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item white-text" href="<?php echo base_url('projeto/sobre') ?>">Sobre</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Contato
        </a>
        <div class="dropdown-menu dropdown-default elegant-color-dark" aria-labelledby="navbarDropdownMenuLink-333">
          <a class="dropdown-item white-text" href="<?php echo base_url('projeto/contato') ?>">Fale Conosco</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://twitter.com/converse_br" target="_blank">
          <i class="fab fa-twitter"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://www.facebook.com/ConverseConsBR/" target="_blank">
          <i class="fab fa-facebook-f"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://www.instagram.com/converse_br/" target="_blank">
          <i class="fab fa-instagram"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="https://www.youtube.com/user/conversebr" target="_blank">
          <i class="fab fa-youtube"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('projeto/alterar') ?>">Alterar</a>
      </li>
    </ul>
  </div>
</nav>
