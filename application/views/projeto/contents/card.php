<div class="card">
    <div class="view overlay">
        <img class="card-img-top" src="<?php echo base_url('assets/images/'. $img .'.jpg'); ?>" alt="Card image cap">
        <a>
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <div class="card-body">
        <h4 class="card-title"><?= $title ?></h4>
        <hr>
        <p class="card-text">
            <?= $descr ?>
        </p>
        <a href="#!" class="black-text d-flex justify-content-end"><h5>Saiba Mais <i class="fas fa-angle-double-right"></i></h5></a>
    </div>
</div>