<div class="col-md-6 mx-auto">
<form class="text-center border border-light p-5" method="post">
    <input type="text" value="<?= isset($produto['tipo']) ? $produto['tipo'] : '' ?>" id="tipo" name="tipo" class="form-control mb-4" placeholder="Tipo">
    (allstar ou kids)
    <input type="text" value="<?= isset($produto['img']) ? $produto['img'] : '' ?>" id="imagem" name="img" class="form-control mb-4" placeholder="Imagem">
    (Informar o nome da imagem, *somente .jpg)
    <input type="text" value="<?= isset($produto['title']) ? $produto['title'] : '' ?>" id="title" name="title" class="form-control mb-4" placeholder="Título">
    <input type="text" value="<?= isset($produto['colecao']) ? $produto['colecao'] : '' ?>" id="colecao" name="colecao" class="form-control mb-4" placeholder="Coleção">
    <input type="text" value="<?= isset($produto['cor']) ? $produto['cor'] : '' ?>" id="cor" name="cor" class="form-control mb-4" placeholder="Cor">
    <input type="text" value="<?= isset($produto['material']) ? $produto['material'] : '' ?>" id="material" name="material" class="form-control mb-4" placeholder="Material">
    <input type="text" value="<?= isset($produto['preco']) ? $produto['preco'] : '' ?>" id="preco" name="preco" class="form-control mb-4" placeholder="Preco">
    <button class="btn btn-red btn-block" type="submit"><?= $acao ?></button>
</form>
</div>