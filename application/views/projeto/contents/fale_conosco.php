<div class="col-md-6 mx-auto">
    <form class="text-center border border-light p-5">
        <p class="h4 mb-4">Fale Conosco</p>
        <input type="text" id="nome" class="form-control mb-4" placeholder="Nome">
        <input type="email" id="email" class="form-control mb-4" placeholder="E-mail">
        <label>Assunto</label>
        <select class="browser-default custom-select mb-4">
            <option value="" disabled>Escolha uma opção</option>
            <option value="1" selected>Elogios</option>
            <option value="2">Dúvidas</option>
            <option value="3">Reclamações</option>
            <option value="4">Sugestões</option>
        </select>
        <div class="form-group">
            <textarea class="form-control rounded-0" id="mensagem" rows="3" placeholder="Mensagem"></textarea>
        </div>
        <button class="btn btn-red btn-block" type="submit">Enviar</button>
    </form>
</div>