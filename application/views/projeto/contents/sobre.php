<div class="container text-center">
    <h1>SOBRE</h1>
    <p> Todo mundo tem uma história. E todo mundo tem uma história que se mistura com a Converse. Seja aquele Chuck Taylor All Star que você ganhou quando ainda era criança ou o Jack Purcell que você comprou para compor o visual antes de um encontro. Até mesmo aquele Cons surrado de show escondido no fundo do armário tem algo para contar; uma história sua e da Converse.
    </p>
    <p> Essas histórias se misturam com as suas e a de muita gente nos nossos mais de 100 anos. Porque foi em algum momento lá em 1908 que tudo isso começou e desde então aprendemos muitas coisas. A principal delas? Não importa o quão velho, mas sim o quão bom você é. Quando você sabe disso, você deixa de contar e começa a escrever a história. Como a gente fez. Com os pés.
    </p>
    <p><strong>O Começo </strong></p>

    <p> Em algum momento de 1908, Marquis Mills Converse decidiu abrir uma empresa de calçados de borracha e ignorou um acordo vigente no mercado que impedia que as companhias fizessem negócio diretamente com os seus varejistas. Os primeiros catálogos se gabavam ao dizer quantos caminhões haviam saído da fábrica da Converse em Malden, Massachusetts, para entregar os produtos diretamente para lojas em Boston. A ideia do senhor Converse deu certo. E o mais importante, ela sobreviveu.
    </p>
    <p>
        <img class="alignnone size-full wp-image-25099 img-fluid" src="http://converse.com.br/wp-content/uploads/2012/09/home1.png" alt="home1" width="800" height="533" />
    </p> &nbsp;
    <p>
        <strong>1908-1918 </strong>
    </p>
    <p> Em 1913, a Converse produziu um catálogo com as seguintes palavras: "Nossa empresa foi fundada em 1908 acreditando profundamente que havia uma grande demanda dos varejistas de tênis por uma companhia que fosse independente o suficiente para não seguir tudo o que todas as outras faziam". Essas palavras se provariam proféticas. E sendo sempre uma marca para aqueles que eram independentes o suficiente para não seguir ninguém, a jovem empresa ocuparia um esporte também jovem: o basquete. A Converse também fabricava pneus, mas os tênis de basquete tinham uma tração melhor.
    </p> &nbsp;
    <p>
        <strong>1918-1928 </strong>
    </p>
    <p> Chuck Taylor se junta à Converse Rubber Company. Como isso aconteceu está perdido na história, mas sua razão não: C huck Taylor era um cara que amava basquete e desejava mais que tudo divulgar por aí esse novo esporte e vender os tênis necessários para praticá-lo. A essa altura, foi introduzido no mercado o tênis All Star, que já tinha como característica a sua sola de borracha com formato de diamantes.
    </p> &nbsp;
    <p>
        <strong>1928-1938</strong>
    </p>
    <p> Quando emprestou seu nome para um modelo de tênis para badminton em 1935, Jack Purcell já tinha ganhado 5 vezes seguidas o campeonato canadense, sendo declarado Campeão Mundial. O tênis Jack Purcell traz até hoje o legado de seu homônimo e toda a sensação desse esporte.
    </p>
    <img class="aligncenter wp-image-19467 img-fluid" title="About Converse" src="http://converse.com.br/wp-content/uploads/2012/09/About_Converse.jpg" alt="About_Converse" width="856" height="473" /> &nbsp;
    <p>
        <strong>1938-1948 </strong>
    </p>
    <p>A Segunda Guerra Mundial trouxe para a Converse uma oportunidade única. Muitos dos produtos destinados aos que serviam o país do outro lado do mundo eram agora o foco da produção. A gama de produtos incluía tênis, roupas, botas para pilotos e soldados do exército, jaquetas e roupas de proteção de borracha e ponchos.
    </p> &nbsp;
    <p>
        <strong>1948-1958 </strong>
    </p>
    <p>A invenção do Rock &amp; Roll. Era alto, lascivo, sujo e tudo mais que os que o temiam diziam que ele era. Também era um movimento em busca de um uniforme, e o encontraram na jaqueta de couro, no jeans azul e no tênis de cano alto. Coincidentemente, bem nessa época o tênis de cano alto existia para ver o nascimento de uma outra coisa: a NBA - National Basketball Association.
    </p>
    <p>
    </p> &nbsp;
    <p>
        <strong>1958-1968 </strong>
    </p>
    <p>Uma década de mudanças pra todo mundo. O Rock &amp; Roll e o basquete cresceram (usando Chucks) e o modelo All Star finalmente ganhou cores. Em tempos tumultuados, nasceram lendas - e de um time de basquete verde e branco para a Invasão Inglesa, dos rooftops das cidades grandes para os campos de alfafa do interior do país, a Converse foi um parceiro de jornada.
    </p>
    <p>
        <img class="alignnone size-full wp-image-25101 img-fluid" src="http://converse.com.br/wp-content/uploads/2012/09/home3.png" alt="home3" width="800" height="533" />
    </p> &nbsp;
    <p>
        <strong>1968-1978</strong>
    </p>
    <p>Em algum momento de 1974 o tênis All Star ganhou uma recauchutagem. Feito em camurça colorida com uma grande e imponente estrela na lateral, era perfeito para o basquete - mas tinha algo a mais naquela dureza e naquele brilho que o tornava irresistível para toda uma geração de roqueiros, skatistas e almas rebeldes. Foi então que em '76 surgiu o Pro Leather, um modelo que se tornou o favorito num momento em que o esporte precisava de uma vibração.
    </p> &nbsp;
    <p>
        <strong>1978-1988</strong>
    </p>
    <p>Imediatamente favorito dentro das quadras, o Weapon se tornou o tênis preferido de times de basquete profissionais e amadores por todo o mundo durante os anos 1980 e 1990. Em 1986, a Converse lançou a campanha "Choose Your Weapon", ou "Escolha Sua Arma", na qual estrelavam dois dos principais jogadores rivais da época usando o modelo que era unanimidade no esporte. Mais de 20 anos depois o legado do Weapon - e seu lugar no esporte e na cultura - continua desafiando os concorrentes.
    </p> &nbsp;
    <p><strong>1988-1998 </strong></p>
    <p>Primeiro veio a campanha Grandmama com um dos principais jogadores da NBA da época. E foi um sucesso. Depois, em 1996, a Converse tinha em suas mãos um tênis de basquete chamado All Star 2000. Foi a primeira tentativa de replicar o Chuck Taylor All Star no ambiente da concorrência atual. E tinha algo a mais naquele logo no tornozelo, na listra vermelha da entressola e na abordagem nada absurda sobre o jogo que ao menos 1 milhão de pessoas não puderam resistir. </p> &nbsp;
    <p><strong>1998-2014 </strong></p>
    <p>A marca entra no seu segundo século honrando sua essência de sempre olhar para as coisas de um jeito um pouco diferente, amando as pessoas que querem mudar o mundo pra melhor e celebrando o espírito de rebeldia e originalidade presente no basquete, no Rock &amp; Roll e onde mais você conseguir encontrar. </p>
    <p>
        <img class="alignnone size-full wp-image-25100 img-fluid" src="http://converse.com.br/wp-content/uploads/2012/09/home2.png" alt="home2" width="800" height="533" />
    </p> &nbsp;
    <p><strong>2015</strong></p>
    <p>Um novo ano, uma nova história. Continuando sua tradição de inovar, a Converse dá o pontapé inicial numa nova fase. E para abrir outro capítulo dessa caminhada, lança o Chuck II. O icônico modelo Chuck Taylor All Star com uma pisada diferente e o visual que nunca saiu e nem sairá dos seus pés. </p>
    <p>E, pelo menos por enquanto, é isso. Por enquanto porque essa história está longe de terminar. As melhores nunca terminam. Elas continuam vivas, respirando por meio da criatividade, da transgressão, do otimismo e da coragem. Sempre evoluindo do que as coisas foram para o que elas são, sem parar de pensar nunca no que elas serão. </p>
</div>