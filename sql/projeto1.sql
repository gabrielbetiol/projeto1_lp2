-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Mar-2019 às 20:56
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `card`
--

CREATE TABLE `card` (
  `id` int(11) NOT NULL,
  `img` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `descr` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `card`
--

INSERT INTO `card` (`id`, `img`, `title`, `descr`, `last_modified`) VALUES
(1, 'victor_sussekind', 'CONS RIDERS BIO: VICTOR SÜSSEKIND', 'Victor Süssekind é o mais novo skatista amador da Converse. Assim como Felipe, o Vitinho, como é conhecido, é daqueles que anda com um sorriso estampado no rosto, sempre.', '2019-03-06 19:18:24'),
(2, 'hentique_crobelatti', 'CONS RIDERS BIO: HENRIQUE CROBELATTI', 'Henrique é daqueles skatistas que fazem as manobras parecerem mais fáceis do que realmente são. Dono de um estilo único e descontraído, Crobs, como é conhecido nas ruas, é um skatista completo: anda em corrimãos grandes e gaps sinistros, misturando …', '2019-03-06 19:18:14'),
(3, 'kaue_cossa', 'CONS RIDERS BIO: KAUÊ COSSA', 'Kauê Cossa pode ser facilmente categorizado um dos três melhores skatistas amadores do Brasil na atualidade. Dono de um estilo único e de um leque gigante de manobras técnicas, Kauê impressiona quando é visto andando de skate. Natural de São …', '2019-03-06 19:26:37'),
(4, 'felipe_oliveira', 'CONS RIDERS BIO: FELIPE OLIVEIRA', 'Idade: 21 anos Categoria: Amador IG: @LOOKDATSH1T Um dos maiores expoentes da nova geração de skatistas, Felipe Oliveira é a personificação da atitude Converse. Natural de Salvador, Bahia, Felipe tem 21 anos, cria suas próprias regras, tem sua própria cre', '2019-03-06 19:26:37'),
(5, 'biano_bianchin', 'CONS RIDER BR: BIANO BIANCHIN', 'Biano Bianchin é uma lenda do skate brasileiro. Natural de São Leopoldo, Rio Grande so Sul, mas morando em São Paulo há alguns anos, ele já desceu alguns dos maiores corrimãos do Brasil e é conhecido internacionalmente por tudo o …', '2019-03-06 19:26:37'),
(6, 'renato_souza', 'CONS RIDER BR: RENATO SOUZA', 'Renato Souza inicia sua carreira como profissional em 2017 depois de ter realizado grandes feitos como skatista amador. É natural de Curitiba, Paraná, e um dos caras mais estilosos do cenário atual. Tem sua própria crew/marca Tropicalents que já fez …', '2019-03-06 19:26:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `img` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `descr` varchar(255) NOT NULL,
  `colecao` varchar(50) NOT NULL,
  `cor` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `preco` varchar(10) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `tipo`, `img`, `title`, `descr`, `colecao`, `cor`, `material`, `preco`, `last_modified`) VALUES
(1, 'allstar', 'allstar_m_1', 'CHUCK TAYLOR ALL STAR', ' ', 'Converse All Star', 'Azuis', 'Tecido', '119,90', '2019-03-08 13:50:05'),
(2, 'allstar', 'allstar_m_2', 'CHUCK TAYLOR ALL STAR BOOT', ' ', 'Converse All Star', 'Tons de Marrom', 'Tecido', '199,99', '2019-03-08 13:50:12'),
(3, 'allstar', 'allstar_m_3', 'CHUCK TAYLOR ALL STAR', '', 'Converse All Star', 'Pretos e Cinzas', 'Tecido', '149,90', '2019-03-08 14:01:56'),
(4, 'kids', 'kids_m_1', 'CHUCK TAYLOR ALL STAR MY FIRST ALL STAR', '', 'Converse All Star', 'Vermelhos', 'Tecido', '79,90', '2019-03-08 14:01:56'),
(5, 'kids', 'kids_b_2', 'CHUCK TAYLOR ALL STAR', '', 'Converse All Star', 'Pretos e Cinzas', 'Couro Sintético', '179,90', '2019-03-08 14:07:43'),
(6, 'kids', 'kids_b_1', 'CHUCK TAYLOR ALL STAR BORDER 2V', '', 'Converse All Star', 'Pretos e Cinzas', 'Tecido', '109,90', '2019-03-08 14:07:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
